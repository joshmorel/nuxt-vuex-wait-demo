import Vue from 'vue'
import { ToastProgrammatic as Toast } from 'buefy'

import types from '../utils/mutation-types'
import doAsync from '../utils/async-util'

export const state = () => ({
  [types.GET_POST_ASYNC.stateKey]: null,
  [types.GET_POSTS_ASYNC.stateKey]: null
})

const baseUrl ='https://jsonplaceholder.typicode.com/posts'

export const actions = {
  async getPostAsync(store, postId) {
    await doAsync(store, {
      url: baseUrl + '/' + postId,
      mutationTypes: types.GET_POST_ASYNC
    })
  },
  async getPostsAsync(store) {
    await doAsync(store, {
      url: baseUrl,
      mutationTypes: types.GET_POSTS_ASYNC
    })
  }
}

export const mutations = {
  [types.GET_POST_ASYNC.SUCCESS] (state, data) {
    Vue.set(state, [types.GET_POST_ASYNC.stateKey], data)
  },
  [types.GET_POST_ASYNC.FAILURE] (state, error) {
    Toast.open({
      message: error.message,
      type: 'is-danger'
    })
  },

  [types.GET_POSTS_ASYNC.SUCCESS] (state, data) {
    Vue.set(state, [types.GET_POSTS_ASYNC.stateKey], data)
  },
  [types.GET_POSTS_ASYNC.FAILURE] (state, error) {
    Toast.open({
      message: error.message,
      type: 'is-danger'
    })
  }
}
