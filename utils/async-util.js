import axios from 'axios'

function sleepRandom() {
  const ms = 6000 * Math.random()
  return new Promise(resolve => setTimeout(resolve, ms))
}

const doAsync = async (store, { url, mutationTypes }) => {
  store.dispatch('wait/start', mutationTypes.loadingKey, { root: true })

  await sleepRandom()

  try {
    const response = await axios(url)
    store.commit(mutationTypes.SUCCESS, response.data)
  } catch (error) {
    store.commit(mutationTypes.FAILURE, error)
  }
  store.dispatch('wait/end', mutationTypes.loadingKey, { root: true })
}

export default doAsync
