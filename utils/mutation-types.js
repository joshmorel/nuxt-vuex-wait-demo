import _ from 'lodash'

const createAsyncMutation = (type) => ({
  SUCCESS: `${type}_SUCCESS`,
  FAILURE: `${type}_FAILURE`,
  loadingKey: _.camelCase(`${type}_PENDING`),
  stateKey: _.camelCase(`${type}_DATA`),
})

const types = {
  GET_POST_ASYNC: createAsyncMutation('GET_POST'),
  GET_POSTS_ASYNC: createAsyncMutation('GET_POSTS'),
}

export default types
